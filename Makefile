TEX = pdflatex
FILES = $(wildcard *.tex)

all: $(FILES)
	$(TEX) main.tex

clean:
	rm *.log *.pdf *.aux *~ *dvi
